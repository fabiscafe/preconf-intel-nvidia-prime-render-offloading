# Maintainer: Fabian Bornschein <fabiscafe-cat-mailbox-dog-org>
## INFO
## Please DO NOT upload to the Arch User Repository (AUR).
## This package is not qualified to be in there and is only here
## as inspiration and for personal use.
## Thank you.

pkgbase="preconf-intel-nvidia-prime-render-offloading"
pkgname=("preconf-intel-nvidia-prime-render-offloading-common"
        "preconf-intel-nvidia-prime-render-offloading-linux"
        "preconf-intel-nvidia-prime-render-offloading-linux-dkms"
        "preconf-intel-nvidia-prime-render-offloading-linux-lts"
        "preconf-intel-nvidia-prime-render-offloading-linux-zen"
)
pkgver=1.1
pkgrel=1
url="https://gitlab.com/fabis_cafe/preconf-intel-nvidia-prime-render-offloading"
arch=('any')
license=('GPL3')
source=(
    "71-kmod-intel.conf"
    "71-kmod-nouveau-blacklist.conf"
    "71-kmod-nvidia.conf"
    "71-kmod-nvidia-pwm.conf"
    "71-nvidia-mkinitcpio.hook"
    "90-nvidia-prime-powermanagement.rules"
    )
sha512sums=('f1c075e8d9df1f54aa5fe5df501a9c18a4ef1c583533116596f6310de3f9a1b73a57992dd212af0d6f883d45f887f72e85deb8cf484e0f36cc76c82193bb06e4'
            '28c558573fa85a99630deebb6d22942cae9ba043aab8ec51825738fcbe7cc1280d00a35534438908a1ce758f1fc720ac4e6fc98e729160f1508baa3a82d2b698'
            '4785d836ab84decb49f197cb0c546ef18c11954c5b62afdb451902a6870dacce2106149a75f1fae948dd180850727fadae31b2821c9c30d0d8ebc4c302ce07b8'
            'a351c49ad2ccfafe823a3efa5f333f48abaa90ad76edd7e445a311e5a919e39ef57ae4e682be3e6310de6764abb3b21d78cc4afd18667e933b834333d4a3d300'
            '924a112b90aa694c67fec7fc54dd1c2191da005b8c6ebc055382b4d4559de3697ab1d56903f0c51eedced08595d9aa66a49c97840b33993735e1b6df9864d2cf'
            '97aeab612a6b2aa9b223165df321382cc9b55afeca340fed43adcdf16f204a2487ecad058ad49e64b637331add3f586935a93d0fe11e117aca941d47010cceaa')

package_preconf-intel-nvidia-prime-render-offloading-common(){
    pkgdesc="common files for an intel-nvidia prime system"
    depends=("nvidia-prime" "nvidia-settings" "nvidia-utils" "opencl-nvidia" "lib32-nvidia-utils" "lib32-opencl-nvidia")
    optdepends=(
        "preconf-intel-nvidia-prime-render-offloading-linux: Configuration for default Arch Kernel"
        "preconf-intel-nvidia-prime-render-offloading-linux-zen: Configuration for ZEN Kernel"
        "preconf-intel-nvidia-prime-render-offloading-linux-lts: Configuration for LTS Kernel"
        "preconf-intel-nvidia-prime-render-offloading-linux-dkms: Generic Kernel, DKMS Configuration")

    cd "${srcdir}"
    ## Drivers load up on boot
    install -Dm 644 "${srcdir}/71-kmod-intel.conf" "${pkgdir}/usr/lib/modules-load.d/71-kmod-intel.conf"
    install -Dm 644 "${srcdir}/71-kmod-nvidia.conf" "${pkgdir}/usr/lib/modules-load.d/71-kmod-nvidia.conf"
    ## Blacklist and PWM options
    install -Dm 644 "${srcdir}/71-kmod-nouveau-blacklist.conf" "${pkgdir}/usr/lib/modprobe.d/71-kmod-nouveau-blacklist.conf"
    install -Dm 644 "${srcdir}/71-kmod-nvidia-pwm.conf" "${pkgdir}/usr/lib/modprobe.d/71-kmod-nvidia-pwm.conf"
    ## udev rules
    install -Dm 644 "${srcdir}/90-nvidia-prime-powermanagement.rules" "${pkgdir}/usr/lib/udev/rules.d/90-nvidia-prime-powermanagement.rules"
    ## ALPM Hook
    install -Dm 644 "${srcdir}/71-nvidia-mkinitcpio.hook" "${pkgdir}/usr/share/libalpm/hooks/71-nvidia-mkinitcpio.hook"
}

package_preconf-intel-nvidia-prime-render-offloading-linux(){
    pkgdesc="configures an intel-nvidia prime system for linux"
    depends=("linux" "nvidia" "preconf-intel-nvidia-prime-render-offloading-common")
    install=install_kernel
}

package_preconf-intel-nvidia-prime-render-offloading-linux-dkms(){
    pkgdesc="configures an intel-nvidia prime system with dkms"
    depends=("nvidia-dkms" "preconf-intel-nvidia-prime-render-offloading-common")
    install=install_dkms
}

package_preconf-intel-nvidia-prime-render-offloading-linux-lts(){
    pkgdesc="configures an intel-nvidia prime system for linux-lts"
    depends=("linux-lts" "nvidia-lts" "preconf-intel-nvidia-prime-render-offloading-common")
    install=install_kernel
}

package_preconf-intel-nvidia-prime-render-offloading-linux-zen(){
    pkgdesc="configures an intel-nvidia prime system for linux-zen"
    depends=("linux-zen" "linux-zen-headers" "nvidia-dkms" "preconf-intel-nvidia-prime-render-offloading-common")
    install=install_kernel
}
